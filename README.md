# capco-coding-eval

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

I decided to use yeoman as an angular boilerplate to help expedite the app creation process. There are a lot of unused features in this app because of that.

As requested, I did not use bootstrap or any third party styling library to make this table, though I did use jQuery to assist with the horizontal scrolling within the table.

If you want to test this with other data sets, there is an HTTP get call in the main.js controller. Right now it is getting a static JSON file with the provided data set.

I chose to focus on functionality instead of building out simple tests with karma due to the time constraints I had for this app, but I do think that TDD is extremely important.

The only other specification that this app is missing is the post request to submit row ID and row status. This was again mainly due to time constraints, but I would implement that as part of the ng-repeat process used to fill out the table. Basically there would be an anchor element with a ng-click listener that would call a function in the controller that would send the post request with the requisite information in the request body.

Please let me know if you have any further questions!

Brandan Hummell

