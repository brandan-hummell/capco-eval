'use strict';

/**
 * @ngdoc function
 * @name capcoCodingEvalApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the capcoCodingEvalApp
 */
angular.module('capcoCodingEvalApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
