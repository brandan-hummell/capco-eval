'use strict';

/**
 * @ngdoc function
 * @name capcoCodingEvalApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the capcoCodingEvalApp
 */
angular.module('capcoCodingEvalApp')
  .controller('MainCtrl', function ($scope, $http) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var thisCTRL = $scope;
    thisCTRL.groupedItems = [];
    thisCTRL.itemsPerPage = 10;
    thisCTRL.pagedItems = [];
    thisCTRL.currentPage = 0;
    thisCTRL.pageSelectorSize = 5;

    $http.get('../../data/sample_data.json')
    .then(function onSuccess(res){
        thisCTRL.tableData = res.data;
        thisCTRL.groupToPages();
    })
    .catch(function onError(res){
      console.error(res);
    });

    thisCTRL.groupToPages = function() {
      thisCTRL.pagedItems = [];

      for(var i = 0; i < thisCTRL.tableData.length; i++) {
        if (i % thisCTRL.itemsPerPage === 0) {
          thisCTRL.pagedItems[Math.floor(i / thisCTRL.itemsPerPage)] = [thisCTRL.tableData[i]];
        } else {
          thisCTRL.pagedItems[Math.floor(i / thisCTRL.itemsPerPage)].push(thisCTRL.tableData[i]);
        }
      }
    };

    thisCTRL.pageRange = function(size, start, end) {
      var pageArray = [];

      if (size < end) {
        end = size;
        start = size - thisCTRL.pageSelectorSize;
      }

      for (var i = start; i < end; i++) {
        pageArray.push(i);
      }
      return pageArray;
    };

    thisCTRL.prevPage = function() {
      if (thisCTRL.currentPage > 0) {
        thisCTRL.currentPage--;
      }
    };

    thisCTRL.nextPage = function() {
      if (thisCTRL.currentPage < thisCTRL.pagedItems.length - 1) {
        thisCTRL.currentPage++;
      }
    };

    thisCTRL.setPage = function () {
      thisCTRL.currentPage = this.n;
    };

    thisCTRL.adjustItemsPerPage = function () {
      thisCTRL.itemsPerPage = this.newItemsPerPage;
      thisCTRL.groupToPages();
    };
    
    
  });
